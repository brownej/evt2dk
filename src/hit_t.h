#ifndef _HIT_T_H_
#define _HIT_T_H_
#include <stdint.h>
#include <vector>

class hit_t {
public:
  uint16_t sourceid;
  uint16_t crateid;
  uint16_t slotid;
  uint16_t chanid;
  uint16_t rawval;
  double   time;
  std::vector<uint16_t>* trace;
public:
  hit_t(uint64_t so, uint64_t cr, uint64_t sl, uint64_t ch, uint64_t v, double t, std::vector<uint16_t>* tr);
};

#endif
