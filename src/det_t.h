#ifndef _DET_T_H_
#define _DET_T_H_

#include <stdint.h>
#include <cstddef>
#include <vector>

class det_t {
protected:
  size_t maxMult;
  uint64_t size;
  std::vector<uint64_t> sourceid;
  std::vector<uint64_t> crateid;
  std::vector<uint64_t> slotid;
  std::vector<uint64_t> chanid;
  std::vector<uint64_t> value;
  std::vector<double> time;
public:
  det_t();
  det_t(size_t s);
public:
  void Reset();
  void reserve(size_t s);
public:
  size_t get_maxMult();
  uint64_t* get_size_ptr();
  uint64_t* get_sourceid_ptr();
  uint64_t* get_crateid_ptr();
  uint64_t* get_slotid_ptr();
  uint64_t* get_chanid_ptr();
  uint64_t* get_value_ptr();
  double* get_time_ptr();
};

#endif
