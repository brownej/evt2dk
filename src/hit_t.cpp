#include <stdint.h>
#include <vector>
#include "hit_t.h"

hit_t::hit_t(uint64_t so, uint64_t cr, uint64_t sl, uint64_t ch, uint64_t v, double t, std::vector<uint16_t>* tr):
  sourceid(so), crateid(cr), slotid(sl), chanid(ch), rawval(v), time(t), trace(tr) {
}
