/*
 *
 */
#include <stdint.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <getopt.h>
#include "det_t.h"
#include "hit_t.h"
#include "event/event_t.h"
#include "event/event_utils.h"
#include "debug.h"
#include "sources.h"

int getArgs(int argc,char **argv);

typedef struct {
  bool batch;
  bool traces;
  bool quiet;
  char filein[100];
  char fileout[100];
  char filesrc[100];
  char name[100];
} gParameters;

int numPhys = 1;
/*
 * Options specified on the command line
 * -e <filename> -- a single .evt input file
 * -i <filename> -- file containing list of input files
 * -o <filename> -- output root file
 * -c <filename> -- source configuration file
 * -n <name>     -- name of the run
 * -t            -- include traces
 * -q            -- quiet; don't print progress
 */
gParameters gParams={true, false, false, "inputs.dat", "out.dk", "sources.cfg", "run"};
bool EVT_DEBUG = false;
std::map<uint64_t, sourcetype_t> sources;

int main (int argc, char *argv[]) {
  FILE *flist = NULL; // File containing list of input filenames
  FILE *fp = NULL;    // Input files
  FILE *fout = NULL;  // Trace output file
  FILE *fsrc = NULL;  // Configuration file for sourceids
  std::vector<std::string> fnames;
  char line[256];
  det_t* all = new det_t();

  // Make sure the args are good
  if (getArgs(argc,argv)!=0) {
    fprintf(stderr,"ERROR: Invalid input!\n");
    exit(-1);
  }

  // Read in sourceids
  if ((fsrc=fopen(gParams.filesrc,"r")) == NULL) {
    fprintf(stderr,"ERROR: Can't open config file '%s'!\n",gParams.filesrc);
    exit(-2);
  }
  while (!feof(fsrc)) {
    uint64_t id;
    char type[100];
    fscanf(fsrc, "%ld\t%s", &id, type);
    if (feof(fsrc)) {
      break;
    }
    if (strcmp(type, "asics") == 0) {
      sources[id] = ASICS;
    } else if (strcmp(type, "ddas") == 0) {
      sources[id] = DDAS;
    } else {
      fprintf(stderr, "Unknown sourceid in configuration file.\n");
    }
  }
  fclose(fsrc);

  // Populate list of filenames
  if (gParams.batch) {
    // Make sure flist is good
    if ((flist=fopen(gParams.filein,"r"))==NULL) {
      fprintf(stderr,"ERROR: Can't open input file '%s'!\n",gParams.filein);
      exit(-2);
    }
    while (fgets(line,256,flist)) {
      for (size_t i = 0; i < 256; ++i) {
        if ((int)(line[i]) == '\n') {
          line[i] = '\0';
        }
      }
      fnames.push_back(std::string(line));
    }
    fclose(flist);
  } else {
    fnames.push_back(std::string(gParams.filein));
  }

  uint64_t nEvt = 0;
  if ((fout = fopen(gParams.fileout,"wb"))==NULL) {
    fprintf(stderr,"ERROR: Can't open output file '%s'!\n",gParams.fileout);
    exit(-2);
  }

  // Write dk header
  uint64_t DK_MAGIC_NUMBER = 0xE2A1642AACB5C4C9;
  uint64_t DK_VERSION_MAJOR = 0;
  uint64_t DK_VERSION_MINOR = 3;
  uint64_t DK_VERSION_PATCH = 0;
  fwrite(&DK_MAGIC_NUMBER, sizeof(uint64_t), 1, fout);
  fwrite(&DK_VERSION_MAJOR, sizeof(uint64_t), 1, fout);
  fwrite(&DK_VERSION_MINOR, sizeof(uint64_t), 1, fout);
  fwrite(&DK_VERSION_PATCH, sizeof(uint64_t), 1, fout);

  uint64_t num_items = 1; // This is 1 because we're only putting in 1 run
  fwrite(&num_items, sizeof(uint64_t), 1, fout);

  if (strlen(gParams.name) > 255) {
    uint64_t l = 0;
    fwrite(&l, sizeof(uint64_t), 1, fout);
  } else {
    uint64_t l = strlen(gParams.name);
    fwrite(&l, sizeof(uint64_t), 1, fout);
    fwrite(gParams.name, sizeof(char), l, fout);
  }
  uint32_t dk_type = 0;
  fwrite(&dk_type, sizeof(uint32_t), 1, fout);

  // Write number of events. We don't know this now, so we need to overwrite
  // this after we know
  long pos_num_evt = ftell(fout);
  fwrite(&nEvt, sizeof(uint64_t), 1, fout);

  // Loop over input files
  for (size_t fnum = 0; fnum < fnames.size(); ++fnum) {
    printf("Reading %s\n", fnames[fnum].c_str());

    if ((fp=fopen(fnames[fnum].c_str(),"r"))==NULL) {
      fprintf(stderr,"Can't open evtfile %s, moving to next file\n", fnames[fnum].c_str());
      continue;
    }

    // Go to the end of file to note where it is, then go back to the beginning
    // This is for printing the progress
    fseek(fp, 0, SEEK_END);
    size_t end = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    while(!feof(fp)) {
      // Print progress through file
      if (!(EVT_DEBUG || gParams.quiet)) {
        printf("%5.2f%%\r", 100.*((double)ftell(fp)/(double)end));
      }

      std::vector<uint8_t> buf;
      size_t index = 0;

      // Read in the size and undo the read (since the size is part of the buffer)
      buf.resize(4);
      fread(buf.data(), sizeof(uint8_t), 4, fp);
      uint64_t size = getBufWord(buf, index, 4);
      // The file might end with some 0s, so don't rewind if that's the case
      if (size != 0) {
        fseek(fp, -4, SEEK_CUR);
      }

      // Read in the whole buffer
      buf.resize(size);
      fread(buf.data(), sizeof(uint8_t), buf.size(), fp);

      if (EVT_DEBUG) {
        printf("%ld\n", buf.size());
        for (size_t buf_i = 0; buf_i < buf.size(); ++ buf_i) {
          printf("%2.2x ", buf[buf_i]);
        }
        printf("\n\n");
      }

      // Parse Buffer
      event_t ev(buf);

      // output to file
      size_t evtsize = ev.hits.size();
      if (evtsize > 0) {
        ++nEvt;
        fwrite(&evtsize, sizeof(uint64_t), 1, fout);
      }
      if (evtsize > all->get_maxMult()) {
        all->reserve(evtsize);
      }
      for (std::vector<hit_t>::iterator it = ev.hits.begin(); it != ev.hits.end(); ++it) {
        size_t*   s  = all->get_size_ptr();
        uint64_t* so = all->get_sourceid_ptr();
        uint64_t* cr = all->get_crateid_ptr();
        uint64_t* sl = all->get_slotid_ptr();
        uint64_t* ch = all->get_chanid_ptr();
        uint64_t* rv = all->get_value_ptr();
        double* time = all->get_time_ptr();

        int16_t di = -1;
        int16_t dc = -1;
        int16_t val = -1;
        double energy_val = nan("");
        double energy_unc = nan("");

        so[*s]   = it->sourceid;
        cr[*s]   = it->crateid;
        sl[*s]   = it->slotid;
        ch[*s]   = it->chanid;
        rv[*s]   = it->rawval;
        time[*s] = it->time;
        ++(*s);

        fwrite(&it->sourceid, sizeof(uint16_t), 1, fout);
        fwrite(&it->crateid,  sizeof(uint16_t), 1, fout);
        fwrite(&it->slotid,   sizeof(uint16_t), 1, fout);
        fwrite(&it->chanid,   sizeof(uint16_t), 1, fout);
        fwrite(&di,           sizeof(uint16_t), 1, fout);
        fwrite(&dc,           sizeof(uint16_t), 1, fout);
        fwrite(&it->rawval,   sizeof(uint16_t), 1, fout);
        fwrite(&val,          sizeof(uint16_t), 1, fout);
        fwrite(&energy_val,   sizeof(double),   1, fout);
        fwrite(&energy_unc,   sizeof(double),   1, fout);
        fwrite(&it->time,     sizeof(double),   1, fout);

        if (gParams.traces && it->trace != NULL && it->trace->size() > 0) {
          size_t trSize = it->trace->size();
          fwrite(&trSize,           sizeof(uint64_t), 1, fout);
          fwrite(it->trace->data(), sizeof(uint16_t), trSize, fout);
        } else {
          size_t trSize = 0;
          fwrite(&trSize, sizeof(uint64_t), 1, fout);
        }
      }

      // Make sure you don't leave junk in the detector data
      all->Reset();
    }

    fclose(fp);
    printf(" Done   \n");
  }

  // Overwrite the beginning of the trace file
  fseek(fout, pos_num_evt, SEEK_SET);
  fwrite(&nEvt, sizeof(uint64_t), 1, fout);
  fclose(fout);

  return 0;
}

int getArgs(int argc, char**argv) {
  char opt;
  int good = 0;

  while ((opt = (char)getopt(argc, argv, "e:i:o:c:n:t:q")) != EOF) {
    switch (opt) {
      case 'e': gParams.batch = false; strcpy(gParams.filein,optarg); break;
      case 'i': gParams.batch = true; strcpy(gParams.filein,optarg); break;
      case 'o': strcpy(gParams.fileout,optarg); break;
      case 'c': strcpy(gParams.filesrc,optarg); break;
      case 'n': strcpy(gParams.name,optarg); break;
      case 't': gParams.traces = true; break;
      case 'q': gParams.quiet = true; break;
    }
  }

  return (good);
}
