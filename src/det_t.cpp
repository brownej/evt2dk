#include "det_t.h"
#include <stdint.h>
#include <vector>

det_t::det_t() {
  reserve(1);
  Reset();
}

det_t::det_t(size_t s) {
  reserve(s);
  Reset();
}

void det_t::Reset() {
  for (size_t i = 0; i < maxMult; ++i) {
    sourceid[i] = 0;
    crateid[i]  = 0;
    slotid[i]   = 0;
    chanid[i]   = 0;
    value[i]    = 0;
    time[i]     = 0;
  }
  size=0;
}

void det_t::reserve(size_t s) {
  if (s > maxMult) {
    maxMult = s;
    sourceid.reserve(s);
    slotid.reserve(s);
    crateid.reserve(s);
    chanid.reserve(s);
    value.reserve(s);
    time.reserve(s);
  }
}

size_t det_t::get_maxMult() {
  return maxMult;
}

uint64_t* det_t::get_size_ptr() {
  return &size;
}

uint64_t* det_t::get_sourceid_ptr() {
  return sourceid.data();
}

uint64_t* det_t::get_crateid_ptr() {
  return crateid.data();
}

uint64_t* det_t::get_slotid_ptr() {
  return slotid.data();
}

uint64_t* det_t::get_chanid_ptr() {
  return chanid.data();
}

uint64_t* det_t::get_value_ptr() {
  return value.data();
}

double* det_t::get_time_ptr() {
  return time.data();
}
