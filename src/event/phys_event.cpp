#include <stdint.h>
#include <vector>
#include "phys_event.h"
#include "event_utils.h"
#include "../debug.h"
#include "../sources.h"

const uint64_t asics_data_chan_t::UNDER_MASK = 0x8000;
const uint64_t asics_data_chan_t::OVER_MASK = 0x4000;
const uint64_t asics_data_chan_t::DATA_MASK = 0x3FFF;
const uint64_t asics_data_chan_t::UNDER_SHIFT = 14;
const uint64_t asics_data_chan_t::OVER_SHIFT = 15;
const uint64_t asics_data_chan_t::DATA_SHIFT = 0;
const uint64_t asics_data_mb_t::CRATE_ID_MASK = 0xF000;
const uint64_t asics_data_mb_t::SLOT_ID_MASK = 0x000F;
const uint64_t asics_data_mb_t::CHAN_ID_MASK = 0x001F;
const uint64_t asics_data_mb_t::CHIP_ID_MASK = 0x1FE0;
const uint64_t asics_data_mb_t::CRATE_ID_SHIFT = 12;
const uint64_t asics_data_mb_t::SLOT_ID_SHIFT = 0;
const uint64_t asics_data_mb_t::CHAN_ID_SHIFT = 0;
const uint64_t asics_data_mb_t::CHIP_ID_SHIFT = 5;
const uint64_t asics_data_t::MARKER_MB1=0xaaaa;
const uint64_t asics_data_t::MARKER_MB2=0xbbbb;
const uint64_t asics_data_t::MARKER_MB3=0xcccc;
const uint64_t asics_data_t::MARKER_MB4=0xdddd;
const uint64_t asics_data_t::MARKER_TS=0xeeee;
const uint64_t ddas_data_t::FINISH_CODE_MASK =     0x80000000;
const uint64_t ddas_data_t::OVERFLOW_MASK =        0x40000000;
const uint64_t ddas_data_t::EVENT_LEN_MASK =       0x3FFE0000;
const uint64_t ddas_data_t::HEADER_LEN_MASK =      0x0001F000;
const uint64_t ddas_data_t::CRATE_ID_MASK =            0x0F00;
const uint64_t ddas_data_t::SLOT_ID_MASK =             0x00F0;
const uint64_t ddas_data_t::CHAN_NO_MASK =             0x000F;
const uint64_t ddas_data_t::CFD_FAIL_MASK =            0x8000;
const uint64_t ddas_data_t::CFD_TRIG_SOURCE_MASK_250 = 0x4000;
const uint64_t ddas_data_t::CFD_TRIG_SOURCE_MASK_500 = 0xE000;
const uint64_t ddas_data_t::CFD_TIME_MASK_100 =        0x7FFF;
const uint64_t ddas_data_t::CFD_TIME_MASK_250 =        0x3FFF;
const uint64_t ddas_data_t::CFD_TIME_MASK_500 =        0x1FFF;
const uint64_t ddas_data_t::FINISH_CODE_SHIFT=31;
const uint64_t ddas_data_t::OVERFLOW_SHIFT=30;
const uint64_t ddas_data_t::EVENT_LEN_SHIFT=17;
const uint64_t ddas_data_t::HEADER_LEN_SHIFT=12;
const uint64_t ddas_data_t::CRATE_ID_SHIFT=8;
const uint64_t ddas_data_t::SLOT_ID_SHIFT=4;
const uint64_t ddas_data_t::CHAN_NO_SHIFT=0;
const uint64_t ddas_data_t::CFD_FAIL_SHIFT=15;
const uint64_t ddas_data_t::CFD_TRIG_SOURCE_SHIFT_250=14;
const uint64_t ddas_data_t::CFD_TRIG_SOURCE_SHIFT_500=13;
const uint64_t ddas_data_t::CFD_TIME_SHIFT=0;

frag_header_t::frag_header_t(const std::vector<uint8_t>& buf, size_t& idx){
  tStamp = getBufWord(buf, idx, 8);
  sourceID = getBufWord(buf, idx, 4);
  size = getBufWord(buf, idx, 4);
  barrierType = getBufWord(buf, idx, 4);

  if (EVT_DEBUG) {
    printf(" ==FRAGMENT HEADER==\n");
    printf(" |TSTAMP: %ld\n", tStamp);
    printf(" |SOURCE_ID: %ld\n", sourceID);
    printf(" |SIZE: %ld\n", size);
    printf(" |BARRIER_TYPE: %ld\n", barrierType);
  }
}

fragment_t::fragment_t(const std::vector<uint8_t>& buf, size_t& idx){
  if (EVT_DEBUG) {
    printf(" ==FRAGMENT==\n");
  }

  fragHeader = NULL;
  fragHeader = new frag_header_t(buf, idx);

  size_t idx_start = idx;
  size = getBufWord(buf, idx, 4);
  type = getBufWord(buf, idx, 4);

  if (EVT_DEBUG) {
    printf(" |SIZE: %ld\n", size);
    printf(" |TYPE: %ld\n", type);
  }

  bodyHeader = NULL;
  bodyHeader = new body_header_t(buf, idx);
  payload = NULL;
  if (sources[fragHeader->sourceID] == ASICS) {
    payload = new asics_payload_t(buf, idx);
  } else if (sources[fragHeader->sourceID] == DDAS) {
    payload = new ddas_payload_t(buf, idx);
  } else {
    fprintf(stderr, "Unknown sourceID(%08lx). Don't know how to parse.\n", fragHeader->sourceID);
    idx = idx_start + size;
  }
}

fragment_t::~fragment_t() {
  if (fragHeader != NULL) {
    delete fragHeader;
  }
  if (bodyHeader != NULL) {
    delete bodyHeader;
  }
  if (payload != NULL) {
    delete payload;
  }
}

asics_payload_t::asics_payload_t(const std::vector<uint8_t>& buf, size_t& idx){
  // multiply size by two because asics reports size as number of 16-bit words
  // also, it is non-inclusive
  size = getBufWord(buf, idx, 2)*2;
  size_t idx_start = idx;

  if (EVT_DEBUG) {
    printf(" ==ASICS PAYLOAD==\n");
    printf(" |SIZE: %ld\n", size);
  }

  // If the size is too big, just bail
  if (!(size > buf.size()-idx)) {
    while (idx-idx_start < size) {
      uint64_t marker = getBufWord(buf, idx, 2, false);
      if ((marker == asics_data_t::MARKER_MB1)
          || (marker == asics_data_t::MARKER_MB2)
          || (marker == asics_data_t::MARKER_MB3)
          || (marker == asics_data_t::MARKER_MB4)) {
        data.push_back(new asics_data_mb_t(buf, idx));
      } else if (marker == asics_data_t::MARKER_TS) {
        data.push_back(new asics_data_ts_t(buf, idx));
      } else {
        // Bail on reading the event
        idx = buf.size();
      }
    }
  }
}

uint64_t asics_payload_t::get_time() {
  for (size_t i = 0; i < data.size(); ++i) {
    if (data[i]->marker == asics_data_t::MARKER_TS) {
      // The ASICs timestamp module measures the count of 40 ns ticks
      return 40*((asics_data_ts_t*) data[i])->tStamp;
    }
  }
  // return 0 if there was no timestamp module
  return 0;
}

payload_t::~payload_t() {
}

asics_payload_t::~asics_payload_t() {
  for (size_t i = 0; i < data.size(); ++i) {
    if (data[i] != NULL) {
      delete data[i];
    }
  }
}

asics_data_mb_t::asics_data_mb_t(const std::vector<uint8_t>& buf, size_t& idx){
  marker = getBufWord(buf, idx, 2);
  uint64_t temp = getBufWord(buf, idx, 2);
  crateID = (temp & CRATE_ID_MASK) >> CRATE_ID_SHIFT;
  slotID = (temp & SLOT_ID_MASK) >> SLOT_ID_SHIFT;
  length = getBufWord(buf, idx, 4)*2;
  size_t idx_start = idx;
  numChans = getBufWord(buf, idx, 2);
  tStamp = getBufWord(buf, idx, 8);

  if (EVT_DEBUG) {
    printf("   ==ASICS MB DATA==\n");
    printf("   |MARKER: %4.4lx\n", marker);
    printf("   |CRATE_ID: %ld\n", crateID);
    printf("   |SLOT_ID: %ld\n", slotID);
    printf("   |LENGTH: %ld\n", length);
    printf("   |NUM_CHANS: %ld\n", numChans);
    printf("   |TSTAMP: %ld\n", tStamp);
  }

  for (size_t i = 0; i < numChans; ++i) {
    chans.push_back(new asics_data_chan_t(buf, idx));
  }
  while (idx-idx_start < length) {
    junk.push_back(getBufWord(buf, idx, 2));
  }

  if (EVT_DEBUG) {
    printf("   |JUNK: ");
    for (size_t i = 0; i < junk.size(); ++i) {
      printf("%4.4lx ", junk[i]);
    }
    printf("\n");
  }
}

asics_data_mb_t::~asics_data_mb_t() {
  for (size_t i = 0; i < chans.size(); ++i) {
    if (chans[i] != NULL) {
      delete chans[i];
    }
  }
}

asics_data_ts_t::asics_data_ts_t(const std::vector<uint8_t>& buf, size_t& idx){
  marker = getBufWord(buf, idx, 2);
  buffer_data_t temp = {0};
  temp.bytes[0] = buf[idx];
  temp.bytes[1] = buf[idx+1];
  temp.bytes[2] = buf[idx+2];
  temp.bytes[3] = buf[idx+3];
  temp.bytes[4] = buf[idx+8];
  temp.bytes[5] = buf[idx+9];
  idx += 12;
  tStamp = temp.all;

  if (EVT_DEBUG) {
    printf("   ==ASICS TS DATA==\n");
    printf("   |MARKER: %4.4lx\n", marker);
    printf("   |TSTAMP: %ld\n", tStamp);
  }
}

asics_data_t::~asics_data_t() {
}

asics_data_ts_t::~asics_data_ts_t() {
}

asics_data_chan_t::asics_data_chan_t(const std::vector<uint8_t>& buf, size_t& idx){
  uint64_t temp = getBufWord(buf, idx, 2);
  chipID = (temp & asics_data_mb_t::CHIP_ID_MASK) >> asics_data_mb_t::CHIP_ID_SHIFT;
  chanID = (temp & asics_data_mb_t::CHAN_ID_MASK) >> asics_data_mb_t::CHAN_ID_SHIFT;
  temp = getBufWord(buf, idx, 2);
  underflow = temp&UNDER_MASK >> UNDER_SHIFT;
  overflow = temp&OVER_MASK >> OVER_SHIFT;
  energy = temp&DATA_MASK >> DATA_SHIFT;
  tStamp = getBufWord(buf, idx, 2);

  if (EVT_DEBUG) {
    printf("     ==ASICS CHAN DATA==\n");
    printf("     |CHIP_ID: %ld\n", chipID);
    printf("     |CHAN_ID: %ld\n", chanID);
    printf("     |UNDER: %d\n", underflow);
    printf("     |OVER: %d\n", overflow);
    printf("     |ENERGY: %ld\n", energy);
    printf("     |TSTAMP: %ld\n", tStamp);
  }
}

ddas_payload_t::ddas_payload_t(const std::vector<uint8_t>& buf, size_t& idx){
  // multiply size by two because asics reports size as number of 16-bit words
  // also, it inclusive
  size_t idx_start = idx;
  size = getBufWord(buf, idx, 4)*2;

  if (EVT_DEBUG) {
    printf(" ==DDAS PAYLOAD==\n");
    printf(" |SIZE: %ld\n", size);
  }

  while (idx-idx_start < size) {
    data.push_back(new ddas_data_t(buf, idx));
  }
}

ddas_payload_t::~ddas_payload_t() {
  for (size_t i = 0; i < data.size(); ++i) {
    if (data[i] != NULL) {
      delete data[i];
    }
  }
}

ddas_data_t::ddas_data_t(const std::vector<uint8_t>& buf, size_t& idx){
  modMSPS = getBufWord(buf, idx, 2);
  idx += 2;
  size_t idx_start = idx;
  uint64_t temp = getBufWord(buf, idx, 4);
  finishCode = (temp & FINISH_CODE_MASK) >> FINISH_CODE_SHIFT;
  overflow = (temp & OVERFLOW_MASK) >> OVERFLOW_SHIFT;
  eventLen = ((temp & EVENT_LEN_MASK) >> EVENT_LEN_SHIFT)*2;
  headerLen = ((temp & HEADER_LEN_MASK) >> HEADER_LEN_SHIFT)*2;
  crateID = (temp & CRATE_ID_MASK) >> CRATE_ID_SHIFT;
  slotID = (temp & SLOT_ID_MASK) >> SLOT_ID_SHIFT;
  chanNo = (temp & CHAN_NO_MASK) >> CHAN_NO_SHIFT;

  tStamp = getBufWord(buf, idx, 6);
  uint64_t cfd = getBufWord(buf, idx, 2);
  energy = getBufWord(buf, idx, 2);
  traceLen = getBufWord(buf, idx, 2)*2;

  // Module specific stuff
  if (modMSPS == 100) {
    cfdFail = (cfd & CFD_FAIL_MASK) >> CFD_FAIL_SHIFT;
    cfdTrigSource = 0;
    cfdTime = (cfd & CFD_TIME_MASK_100) >> CFD_TIME_SHIFT;
    time = 10.*((((double)cfdTime)/32768.) + ((double)tStamp));
  } else if (modMSPS == 250) {
    cfdFail = (cfd & CFD_FAIL_MASK) >> CFD_FAIL_SHIFT;
    cfdTrigSource = (cfd & CFD_TRIG_SOURCE_MASK_250) >> CFD_TRIG_SOURCE_SHIFT_250;
    cfdTime = (cfd & CFD_TIME_MASK_250) >> CFD_TIME_SHIFT;
    time = (4.*(((double)cfdTime)/16384. - (double)cfdTrigSource) + 8.*((double)tStamp));
  } else if (modMSPS == 500) {
    cfdFail = 0;
    cfdTrigSource = (cfd & CFD_TRIG_SOURCE_MASK_500) >> CFD_TRIG_SOURCE_SHIFT_500;
    cfdTime = (cfd & CFD_TIME_MASK_500) >> CFD_TIME_SHIFT;
    time = (2.*(((double)cfdTime)/16384. + (double)cfdTrigSource - 1.) + 10.*((double)tStamp));
  } else {
    fprintf(stderr, "Unknown modMSPS for DDAS: %ld.\n", modMSPS);
  }

  // Longer Headers
  // TODO: Right now, we just store the rest in header, instead of parsing
  while (idx-idx_start < headerLen) {
    header.push_back(getBufWord(buf, idx, 2));
  }

  idx_start = idx;
  while (idx-idx_start < traceLen) {
    trace.push_back(getBufWord(buf, idx, 2));
  }

  if (EVT_DEBUG) {
    printf("   ==DDAS DATA==\n");
    printf("   |MOD_MSPS: %ld\n", modMSPS);
    printf("   |FINISH_CODE: %ld\n", finishCode);
    printf("   |OVERFLOW: %ld\n", overflow);
    printf("   |EVENT_LENGTH: %ld\n", eventLen);
    printf("   |HEADER_LENGTH: %ld\n", headerLen);
    printf("   |CRATE_ID: %ld\n", crateID);
    printf("   |SLOT_ID: %ld\n", slotID);
    printf("   |CHAN_NO: %ld\n", chanNo);
    printf("   |TSTAMP: %ld\n", tStamp);
    printf("   |CFD_FAIL: %ld\n", cfdFail);
    printf("   |CFD_TRIG_SOURCE: %ld\n", cfdTrigSource);
    printf("   |CFD_FRACTIONAL_TIME: %ld\n", cfdTime);
    printf("   |ENERGY: %ld\n", energy);
    printf("   |TRACE_LENGTH: %ld\n", traceLen);
    printf("   |REST_OF_HEADER: ");
    for (size_t i = 0; i < header.size(); ++i) {
      printf("%4.4lx ", header[i]);
    }
    printf("\n");
    printf("   |TRACE: ");
    for (size_t i = 0; i < trace.size(); ++i) {
      printf("%4.4x ", trace[i]);
    }
    printf("\n");
  }
}
