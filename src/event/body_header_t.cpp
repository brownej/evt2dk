#include "body_header_t.h"
#include "event_utils.h"
#include "../debug.h"

body_header_t::body_header_t(const std::vector<uint8_t>& buf, size_t& idx) {
  size = getBufWord(buf, idx, 4);

  if(EVT_DEBUG) {
    printf(" ==BODY HEADER==\n");
    printf(" |SIZE: %ld\n", size);
  }

  if (size == 0) {
    // This is just weird
  } else if (size == 20) {
    tStamp = getBufWord(buf, idx, 8);
    sourceID = getBufWord(buf, idx, 4);
    barrierType = getBufWord(buf, idx, 4);

    if(EVT_DEBUG) {
      printf(" |TSTAMP: %ld\n", tStamp);
      printf(" |SOURCE_ID: %ld\n", sourceID);
      printf(" |BARRIER_TYPE: %ld\n", barrierType);
    }
  } else {
    // Invalid body header size
  }
}
