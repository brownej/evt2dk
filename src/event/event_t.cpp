#include <stdint.h>
#include <vector>
#include <cstdio>
#include "body_header_t.h"
#include "event_t.h"
#include "event_utils.h"
#include "ring_items.h"
#include "../debug.h"
#include "../hit_t.h"
#include "../sources.h"

event_t::event_t(const std::vector<uint8_t>& buf) {
  size_t idx = 0;
  size = getBufWord(buf, idx, 4);
  type = getBufWord(buf, idx, 4);

  if (EVT_DEBUG) {
    printf("\n==BEGIN EVENT==\n");
    printf("SIZE: %ld\n", size);
    printf("RING ITEM TYPE: %ld\n", type);
  }

  bodyHeader = NULL;
  bodyHeader = new body_header_t(buf, idx);

  ringItem = NULL;
  if ((type == ring_item_t::BEGIN_RUN)
      || (type == ring_item_t::END_RUN)
      || (type == ring_item_t::PAUSE_RUN)
      || (type == ring_item_t::RESUME_RUN)
      || (type == ring_item_t::ABNORMAL_ENDRUN)) {
    ringItem = new ri_state_transition_t(buf, idx, type);
  } else if ((type == ring_item_t::PACKET_TYPES)
      || (type == ring_item_t::MONITORED_VARIABLES)) {
    ringItem = new ri_text_t(buf, idx, type);
  } else if (type == ring_item_t::RING_FORMAT) {
    ringItem = new ri_data_format_t(buf, idx, type);
  } else if (type == ring_item_t::PERIODIC_SCALERS) {
    ringItem = new ri_scalers_t(buf, idx, type);
  } else if (type == ring_item_t::PHYSICS_EVENT) {
    ringItem = new ri_phys_event_t(buf, idx, type);
  } else if (type == ring_item_t::PHYSICS_EVENT_COUNT) {
    ringItem = new ri_phys_event_count_t(buf, idx, type);
  } else if ((type == ring_item_t::EVB_FRAGMENT)
      || (type == ring_item_t::EVB_UNKNOWN_PAYLOAD)) {
    ringItem = new ri_evb_t(buf, idx, type);
  } else if (type == ring_item_t::EVB_GLOM_INFO) {
    ringItem = new ri_evb_glom_info_t(buf, idx, type);
  } else if (type == ring_item_t::FIRST_USER_ITEM_CODE) {
    ringItem = new ri_first_user_item_t(buf, idx, type);
  } else {
    if (EVT_DEBUG) {
      printf(" ==UNKNOWN_TYPE==\n");
    }
  }
  if (EVT_DEBUG) {
    printf("==END EVENT==\n");
  }

  pop_hits();
}

event_t::~event_t() {
  if (bodyHeader != NULL) {
    delete bodyHeader;
  }
  if (ringItem != NULL) {
    delete ringItem;
  }
}

void event_t::pop_hits() {
  if (type == ring_item_t::PHYSICS_EVENT) {
    ri_phys_event_t* ri = (ri_phys_event_t*) ringItem;
    for (size_t i = 0; i < ri->frags.size(); ++i) {
      if (sources[ri->frags[i]->fragHeader->sourceID] == ASICS) {
        asics_payload_t* payload = (asics_payload_t*) ri->frags[i]->payload;
        for (size_t j = 0; j < payload->data.size(); ++j) {
          if ((payload->data[j]->marker == asics_data_t::MARKER_MB1)
              || (payload->data[j]->marker == asics_data_t::MARKER_MB2)
              || (payload->data[j]->marker == asics_data_t::MARKER_MB3)
              || (payload->data[j]->marker == asics_data_t::MARKER_MB4)) {
            asics_data_mb_t* mbData = (asics_data_mb_t*) payload->data[j];

            for (size_t k = 0; k < mbData->chans.size(); ++k) {
              asics_data_chan_t* chanData = mbData->chans[k];

              uint64_t  so = ri->frags[i]->fragHeader->sourceID;
              uint64_t  cr = mbData->slotID-2;
              uint64_t  sl = chanData->chipID-1;
              uint64_t  ch = chanData->chanID;
              uint64_t val = chanData->energy;
              double  time = payload->get_time();

              hits.push_back(hit_t(so, cr, sl, ch, val, time, NULL));
            }
          }
        }
      } else if (sources[ri->frags[i]->fragHeader->sourceID] == DDAS) {
        ddas_payload_t* payload = (ddas_payload_t*) ri->frags[i]->payload;

        for (size_t j = 0; j < payload->data.size(); ++j) {
          ddas_data_t* data = (ddas_data_t*) payload->data[j];

          uint64_t  so = ri->frags[i]->fragHeader->sourceID;
          uint64_t  cr = data->crateID;
          uint64_t  sl = data->slotID-1;
          uint64_t  ch = data->chanNo;
          uint64_t val = data->energy;
          double  time = data->time;

          hits.push_back(hit_t(so, cr, sl, ch, val, time, &data->trace));
        }
      }
    }
  }
}
