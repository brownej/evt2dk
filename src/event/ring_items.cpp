#include <stdint.h>
#include <cstddef>
#include <vector>
#include <stdint.h>
#include <cstdio>
#include <cstring>
#include <string>
#include "event_utils.h"
#include "ring_items.h"
#include "body_header_t.h"
#include "body_header_t.h"
#include "../debug.h"

const uint64_t ring_item_t::BEGIN_RUN            = 1;
const uint64_t ring_item_t::END_RUN              = 2;
const uint64_t ring_item_t::PAUSE_RUN            = 3;
const uint64_t ring_item_t::RESUME_RUN           = 4;
const uint64_t ring_item_t::ABNORMAL_ENDRUN      = 5;
const uint64_t ring_item_t::PACKET_TYPES         = 10;
const uint64_t ring_item_t::MONITORED_VARIABLES  = 11;
const uint64_t ring_item_t::RING_FORMAT          = 12;
const uint64_t ring_item_t::PERIODIC_SCALERS     = 20;
const uint64_t ring_item_t::PHYSICS_EVENT        = 30;
const uint64_t ring_item_t::PHYSICS_EVENT_COUNT  = 31;
const uint64_t ring_item_t::EVB_FRAGMENT         = 40;
const uint64_t ring_item_t::EVB_UNKNOWN_PAYLOAD  = 41;
const uint64_t ring_item_t::EVB_GLOM_INFO        = 42;
const uint64_t ring_item_t::FIRST_USER_ITEM_CODE = 32768;

ring_item_t::~ring_item_t() {
}

ri_state_transition_t::ri_state_transition_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type = rType;
  runNo = getBufWord(buf, idx, 4);
  tOffset = getBufWord(buf, idx, 4);
  tStamp = getBufWord(buf, idx, 4);
  offsetDiv = getBufWord(buf, idx, 4);
  title.assign((char*)(buf.data())+idx, 80);
  idx += 80;

  if (EVT_DEBUG) {
    if (type == ring_item_t::BEGIN_RUN) {
      printf(" ==BEGIN_RUN==\n");
    } else if (type == ring_item_t::END_RUN) {
      printf(" ==END_RUN==\n");
    } else if (type == ring_item_t::PAUSE_RUN) {
      printf(" ==PAUSE_RUN==\n");
    } else if (type == ring_item_t::RESUME_RUN) {
      printf(" ==RESUME_RUN==\n");
    } else if (type == ring_item_t::ABNORMAL_ENDRUN) {
      printf(" ==ABNORMAL_ENDRUN==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |RUN_NUMBER: %ld\n", runNo);
    printf(" |TIME_OFFSET: %ld\n", tOffset);
    printf(" |TIMESTAMP: %ld\n", tStamp);
    printf(" |OFFSET_DIVISOR: %ld\n", offsetDiv);
    printf(" |TITLE: %s\n", title.c_str());
  }
}

ri_text_t::ri_text_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type= rType;
  tOffset = getBufWord(buf, idx, 4);
  tStamp = getBufWord(buf, idx, 4);
  stringCount = getBufWord(buf, idx, 4);
  offsetDiv = getBufWord(buf, idx, 4);
  for (size_t i = 0; i < stringCount; ++i) {
    strs.push_back(std::string((char*)buf.data()+idx));
    idx += strlen((char*)buf.data()+idx) + 1;
  }

  if (EVT_DEBUG) {
    if (type == ring_item_t::PACKET_TYPES) {
      printf(" ==PACKET_TYPES==\n");
    } else if (type == ring_item_t::MONITORED_VARIABLES) {
      printf(" ==MONITORED_VARIABLES==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |TIME_OFFSET: %ld\n", tOffset);
    printf(" |TIMESTAMP: %ld\n", tStamp);
    printf(" |STRING_COUNT: %ld\n", stringCount);
    printf(" |OFFSET_DIVISOR: %ld\n", offsetDiv);
    for (size_t i = 0; i < stringCount; ++i) {
      printf(" |  STRING: ");
      printf("%s\n", strs[i].c_str());
    }
  }
}

ri_data_format_t::ri_data_format_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type = rType;
  majorVer = getBufWord(buf, idx, 2);
  minorVer = getBufWord(buf, idx, 2);

  if (EVT_DEBUG) {
    if (type == ring_item_t::RING_FORMAT) {
      printf(" ==RING_FORMAT==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |VERSION: %ld.%ld\n", majorVer, minorVer);
  }
}

ri_scalers_t::ri_scalers_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type= rType;
  startOffset = getBufWord(buf, idx, 4);
  stopOffset = getBufWord(buf, idx, 4);
  tStamp = getBufWord(buf, idx, 4);
  interDiv =getBufWord(buf, idx, 4);
  scalerCount = getBufWord(buf, idx, 4);
  isIncremental = getBufWord(buf, idx, 4);
  for (size_t i = 0; i < scalerCount; ++i) {
    scals.push_back(getBufWord(buf, idx, 4));
  }

  if (EVT_DEBUG) {
    if (type == ring_item_t::PERIODIC_SCALERS) {
      printf(" ==PERIODIC_SCALERS==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |INTERVAL_START_OFFSET: %ld\n", startOffset);
    printf(" |INTERVAL_STOP_OFFSET: %ld\n", stopOffset);
    printf(" |TIMESTAMP: %ld\n", tStamp);
    printf(" |INTERVAL_DIVISOR: %ld\n", interDiv);
    printf(" |SCALER_COUNT: %ld\n", scalerCount);
    printf(" |IS_INCREMENTAL: %ld\n", isIncremental);
    for (size_t i = 0; i < scalerCount; ++i) {
      printf(" |  SCALER: %ld\n", scals[i]);
    }
  }
}

ri_phys_event_t::ri_phys_event_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type = rType;
  size_t idx_start = idx; // size is inclusive, so set idx_start before getting size
  size = getBufWord(buf, idx, 4);

  if (EVT_DEBUG) {
    if (type == ring_item_t::PHYSICS_EVENT) {
      printf("\n ==PHYSICS_EVENT==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |SIZE: %ld\n", size);
  }

  while (idx-idx_start < size) {
    frags.push_back(new fragment_t(buf, idx));
  }

  if (EVT_DEBUG) {
    printf(" |DATA: ");
    for(size_t i = idx_start; i < buf.size(); i+=2) {

      buffer_data_t d = {0};
      d.bytes[0] = buf[i];
      d.bytes[1] = buf[i+1];

      printf("%4.4lx ", d.all);
    }
    printf("\n");
  }
}

ri_phys_event_t::~ri_phys_event_t() {
  for (size_t i = 0; i < frags.size(); ++i) {
    if (frags[i] != NULL) {
      delete frags[i];
    }
  }
}

ri_phys_event_count_t::ri_phys_event_count_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type = rType;
  tOffset = getBufWord(buf, idx, 4);
  offsetDiv = getBufWord(buf, idx, 4);
  tStamp = getBufWord(buf, idx, 4);
  eventCount = getBufWord(buf, idx, 8);

  if (EVT_DEBUG) {
    if (type == ring_item_t::PHYSICS_EVENT_COUNT) {
      printf(" ==PHYSICS_EVENT_COUNT==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |TIME_OFFSET: %ld\n", tOffset);
    printf(" |OFFSET_DIVISOR: %ld\n", offsetDiv);
    printf(" |TIMESTAMP: %ld\n", tStamp);
    printf(" |EVENT_COUNT: %ld\n", eventCount);
  }
}

//TODO: Handle this type
ri_evb_t::ri_evb_t(const std::vector<uint8_t>& buf __attribute__((unused)), size_t& idx __attribute__((unused)), const uint64_t rType) {
  type = rType;

  if (EVT_DEBUG) {
    if (type == ring_item_t::EVB_FRAGMENT) {
      printf(" ==EVB_FRAGMENT==\n");
    } else if (type == ring_item_t::EVB_UNKNOWN_PAYLOAD) {
      printf(" ==EVB_UNKNOWN_PAYLOAD==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
  }
}

ri_evb_glom_info_t::ri_evb_glom_info_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType) {
  type=rType;
  coincTicks = getBufWord(buf, idx, 8);
  isBuilding = getBufWord(buf, idx, 2);
  tsPolicy = getBufWord(buf, idx, 2);

  if (EVT_DEBUG) {
    if (type == ring_item_t::EVB_GLOM_INFO) {
      printf(" ==EVB_GLOM_INFO==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
    printf(" |COINCIDENT_TICKS: %ld\n", coincTicks);
    printf(" |IS_BUILDING: %d\n", isBuilding);
    printf(" |TS_POLICY: %d\n", tsPolicy);
  }
}

ri_first_user_item_t::ri_first_user_item_t(const std::vector<uint8_t>& buf __attribute__((unused)), size_t& idx __attribute__((unused)), const uint64_t rType) {
  type=rType;

  if (EVT_DEBUG) {
    if (type == ring_item_t::FIRST_USER_ITEM_CODE) {
      printf(" ==FIRST_USER_ITEM_CODE==\n");
    } else {
      printf(" ==UNKNOWN_TYPE==\n");
    }
  }
}
