#ifndef _RING_ITEMS_H_
#define _RING_ITEMS_H_

#include <stdint.h>
#include <cstddef>
#include <vector>
#include <string>
#include "body_header_t.h"
#include "phys_event.h"

/* Ring Item Types
 *
 * See http://docs.nscl.msu.edu/daq/newsite/nscldaq-11.1/x312.html
 * for a description of the different types.
 *
 * The only valid timestamps are those in PHYSICS_EVENT
 */
class ring_item_t {
public:
  uint64_t type;
public:
  static const uint64_t BEGIN_RUN;
  static const uint64_t END_RUN;
  static const uint64_t PAUSE_RUN;
  static const uint64_t RESUME_RUN;
  static const uint64_t ABNORMAL_ENDRUN;
  static const uint64_t PACKET_TYPES;
  static const uint64_t MONITORED_VARIABLES;
  static const uint64_t RING_FORMAT;
  static const uint64_t PERIODIC_SCALERS;
  static const uint64_t PHYSICS_EVENT;
  static const uint64_t PHYSICS_EVENT_COUNT;
  static const uint64_t EVB_FRAGMENT;
  static const uint64_t EVB_UNKNOWN_PAYLOAD;
  static const uint64_t EVB_GLOM_INFO;
  static const uint64_t FIRST_USER_ITEM_CODE;
public:
  virtual ~ring_item_t();
};

class ri_state_transition_t: public ring_item_t {
public:
  uint64_t runNo;
  uint64_t tOffset;
  uint64_t tStamp;
  uint64_t offsetDiv;
  std::string title;

public:
  ri_state_transition_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_text_t: public ring_item_t {
public:
  uint64_t tOffset;
  uint64_t tStamp;
  uint64_t stringCount;
  uint64_t offsetDiv;
  std::vector<std::string> strs;
public:
  ri_text_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_data_format_t: public ring_item_t {
public:
  uint64_t majorVer;
  uint64_t minorVer;
public:
  ri_data_format_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_scalers_t: public ring_item_t {
public:
  uint64_t startOffset;
  uint64_t stopOffset;
  uint64_t tStamp;
  uint64_t interDiv;
  uint64_t scalerCount;
  uint64_t isIncremental;
  std::vector<uint64_t> scals;
public:
  ri_scalers_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_phys_event_t: public ring_item_t {
public:
  uint64_t size;
  std::vector<fragment_t*> frags;
public:
  ri_phys_event_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
  ~ri_phys_event_t();
};

class ri_phys_event_count_t: public ring_item_t {
public:
  uint64_t tOffset;
  uint64_t offsetDiv;
  uint64_t tStamp;
  uint64_t eventCount;
public:
  ri_phys_event_count_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_evb_t: public ring_item_t {
public:
public:
  ri_evb_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_evb_glom_info_t: public ring_item_t {
public:
  uint64_t coincTicks;
  uint16_t isBuilding;
  uint16_t tsPolicy;
public:
  ri_evb_glom_info_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};

class ri_first_user_item_t: public ring_item_t {
public:
public:
  ri_first_user_item_t(const std::vector<uint8_t>& buf, size_t& idx, const uint64_t rType);
};
#endif
