#ifndef _BODY_HEADER_T_H_
#define _BODY_HEADER_T_H_

/* body_header_t
 *
 * Body headers may more may not contain some useful information.
 * They are found all over the place in events and ring items.
 * There are two types of body headers.
 * 
 * Type 1: Empty
 * This type has no information.
 * size = 0 (non-inclusive, obviously)
 * And that's it.
 *
 * Type 2: Not Empty
 * This type does have information.
 * size = 20 (inclusive)
 */

#include <cstddef>
#include <stdint.h>
#include <vector>
#include <cstdio>

class body_header_t {
public:
  uint64_t size;
  uint64_t tStamp;
  uint64_t sourceID;
  uint64_t barrierType;
public:
  body_header_t(const std::vector<uint8_t>& buf, size_t& idx);
};

#endif
