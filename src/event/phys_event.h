#ifndef _PHYS_EVENT_H_
#define _PHYS_EVENT_H_

/* phys_event
 *
 * This file is for parsing PHYSICS_EVENT ring items.
 * These are the most important ring items because they contain your data,
 * but they are also the most complex.
 *
 * The data is generally structured in the following way:
 * -fragment
 *  -fragment header
 *  -size
 *  -type
 *  -payload
 *   -size
 *   -data
 *   -data
 *   -data
 *   -data
 *   -data
 *
 * There may be more than one fragment in an event
 */
#include <vector>
#include <stdint.h>
#include "body_header_t.h"

/* ASICs Data
 *
 * There are two types of ASICs data: Motherboard and Timestamp
 *
 * Motherboard
 * This is the data created by the JTEC XLM modules.
 * It first contains information on that module, then information on the particular channels with data.
 *
 * Timestamp
 * This data is created by the SIS 36/38XX modules.
 * It just has timestamp data.
 */
class asics_data_t {
public:
  uint64_t marker;
public:
  static const uint64_t MARKER_MB1;
  static const uint64_t MARKER_MB2;
  static const uint64_t MARKER_MB3;
  static const uint64_t MARKER_MB4;
  static const uint64_t MARKER_TS;
public:
  virtual ~asics_data_t();
};

class asics_data_chan_t {
public:
  uint64_t chipID;
  uint64_t chanID;
  bool underflow;
  bool overflow;
  uint64_t energy;
  uint64_t tStamp;
public:
  static const uint64_t UNDER_MASK;
  static const uint64_t OVER_MASK;
  static const uint64_t DATA_MASK;
  static const uint64_t UNDER_SHIFT;
  static const uint64_t OVER_SHIFT;
  static const uint64_t DATA_SHIFT;
public:
  asics_data_chan_t(const std::vector<uint8_t>& buf, size_t& idx);
};

class asics_data_mb_t: public asics_data_t {
public:
  uint64_t crateID;
  uint64_t slotID;
  uint64_t length;
  uint64_t numChans;
  uint64_t tStamp;
  std::vector<asics_data_chan_t*> chans;
  std::vector<uint64_t> junk;
public:
  static const uint64_t CRATE_ID_MASK;
  static const uint64_t SLOT_ID_MASK;
  static const uint64_t CHAN_ID_MASK;
  static const uint64_t CHIP_ID_MASK;
  static const uint64_t CRATE_ID_SHIFT;
  static const uint64_t SLOT_ID_SHIFT;
  static const uint64_t CHAN_ID_SHIFT;
  static const uint64_t CHIP_ID_SHIFT;
public:
  asics_data_mb_t(const std::vector<uint8_t>& buf, size_t& idx);
  ~asics_data_mb_t();
};

class asics_data_ts_t: public asics_data_t {
public:
  uint64_t tStamp;
public:
  asics_data_ts_t(const std::vector<uint8_t>& buf, size_t& idx);
  ~asics_data_ts_t();
};

/* DDAS Data
 *
 * This is data from the Pixie-16 modules.
 * There's some data to locate the channel and then some time and energy data.
 * But then there's also optional trace data.
 * There are different modules with very slightly different data structures.
 * This mostly just affects the timestamp.
 */
class ddas_data_t {
public:
  uint64_t modMSPS;
  uint64_t finishCode; 
  uint64_t overflow;
  uint64_t eventLen;
  uint64_t headerLen;
  uint64_t crateID;
  uint64_t slotID;
  uint64_t chanNo;
  uint64_t tStamp;
  uint64_t cfdTime;
  uint64_t cfdFail;
  uint64_t cfdTrigSource;
  uint64_t energy;
  uint64_t traceLen;
  double time;
  std::vector<uint64_t> header;
  std::vector<uint16_t> trace;
public:
  static const uint64_t FINISH_CODE_MASK;
  static const uint64_t OVERFLOW_MASK;
  static const uint64_t EVENT_LEN_MASK;
  static const uint64_t HEADER_LEN_MASK;
  static const uint64_t CRATE_ID_MASK;
  static const uint64_t SLOT_ID_MASK;
  static const uint64_t CHAN_NO_MASK;
  static const uint64_t CFD_FAIL_MASK;
  static const uint64_t CFD_TRIG_SOURCE_MASK_250;
  static const uint64_t CFD_TRIG_SOURCE_MASK_500;
  static const uint64_t CFD_TIME_MASK_100;
  static const uint64_t CFD_TIME_MASK_250;
  static const uint64_t CFD_TIME_MASK_500;
  static const uint64_t FINISH_CODE_SHIFT;
  static const uint64_t OVERFLOW_SHIFT;
  static const uint64_t EVENT_LEN_SHIFT;
  static const uint64_t HEADER_LEN_SHIFT;
  static const uint64_t CRATE_ID_SHIFT;
  static const uint64_t SLOT_ID_SHIFT;
  static const uint64_t CHAN_NO_SHIFT;
  static const uint64_t CFD_FAIL_SHIFT;
  static const uint64_t CFD_TRIG_SOURCE_SHIFT_250;
  static const uint64_t CFD_TRIG_SOURCE_SHIFT_500;
  static const uint64_t CFD_TIME_SHIFT;
public:
  ddas_data_t(const std::vector<uint8_t>& buf, size_t& idx);
};

/* Payload
 *
 * The payload contains a size and then a list of data from the modules.
 * The ASICs size is in number of 16-bit words and is non-inclusive.
 * The DDAS size is in number of 16-bit words and is inclusive.
 * Both are converted to the number of bytes before being saved.
 */
class payload_t {
public:
  uint64_t size;
public:
  virtual ~payload_t();
};

class asics_payload_t: public payload_t {
public:
  std::vector<asics_data_t*> data;
  uint64_t get_time();
public:
  asics_payload_t(const std::vector<uint8_t>& buf, size_t& idx);
  ~asics_payload_t();
};

class ddas_payload_t: public payload_t {
public:
  std::vector<ddas_data_t*> data;
public:
  ddas_payload_t(const std::vector<uint8_t>& buf, size_t& idx);
  ~ddas_payload_t();
};

/* Fragment Header
 *
 * A fragment header is like a body header, but different for some reason.
 * size is the size of the payload.
 * sourceID is very important. This decides what payload to use.
 */
class frag_header_t {
public:
  uint64_t tStamp;
  uint64_t sourceID;
  uint64_t size;
  uint64_t barrierType;
public:
  frag_header_t(const std::vector<uint8_t>& buf, size_t& idx);
};

/* Fragment
 *
 * This is a data fragment. This holds the important stuff.
 * fragHeader.sourceID deteremines which payload to use and thus how to parse the data.
 * size is the number of bytes remaining in the fragment (inclusive).
 */
class fragment_t {
public:
  frag_header_t* fragHeader;
  uint64_t size;
  uint64_t type;
  body_header_t* bodyHeader;
  payload_t* payload;
public:
  fragment_t(const std::vector<uint8_t>& buf, size_t& idx);
  ~fragment_t();
};

#endif
