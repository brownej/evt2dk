#ifndef _EVENT_T_H_
#define _EVENT_T_H_

/* event_t
 *
 * This class represents an event from the ring buffer.
 * There are different types of ring items that an event can hold.
 * Each type of ring item has a specific value for the type variable.
 * Some of the types have the same structure, so they share a class.
 */
#include <vector>
#include <stdint.h>
#include "body_header_t.h"
#include "ring_items.h"
#include "../hit_t.h"

class event_t {
public:
  std::vector<hit_t> hits;
public:
  uint64_t size; // Number of bits of the event (inclusive)
  uint64_t type;
  body_header_t* bodyHeader;
  ring_item_t* ringItem;
public:
  event_t(const std::vector<uint8_t>& buf);
  ~event_t();
protected:
  void pop_hits();
};

#endif
